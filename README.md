# MineClone2 Furniture and decorations mod [mcl_decor]
Adds more decoration and furniture blocks to MineClone 2

[![ContentDB](https://content.minetest.net/packages/rudzik8/mcl_decor/shields/downloads/)](https://content.minetest.net/packages/rudzik8/mcl_decor/)

![Screenshot](screenshot.png "Screenshot")



## Contributing (how can I help?)

### Translate the mod
Just fork this repo, copy "template.txt" and paste as "mcl_decor.XX.tr" where XX is your 2-letter language code (for example, "de" for German/<ins>De</ins>utsch). After that, you can open that file and translate strings to your language.
After the work is done you need to create a pull request, and if it has no problems - I'll merge it!

### Report bugs
Yes! Via "Issues" tab in this repo. I can't fix everything though, but if you found something in my mod that looks like a bug - report it.

### Share mod with friends and make reviews
On ContentDB of course. Reviews motivate me to continue updating this mod. Also, you can star this repo on MeseHub as well!

### Help me with the code
I'm not a very good coder so if you can - please, fix some bugs or maybe even add something via pull requests!

**Thanks.**



## License
Code licensed under GPLv3, see LICENSE file for details.
Media licensed under CC-BY-SA 4.0, see [this deed](https://creativecommons.org/licenses/by-sa/4.0/) for details.



## Credits
**For assets:**

- [Coalquartz Tile texture](textures/mcl_decor_coalquartz_tile.png) uses [coal block](https://git.minetest.land/MineClone2/MineClone2/src/branch/master/mods/ITEMS/mcl_core/textures/default_coal_block.png) and [quartz block](https://git.minetest.land/MineClone2/MineClone2/src/branch/master/mods/ITEMS/mcl_nether/textures/mcl_nether_quartz_block_side.png) textures under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) by [XSSheep](https://github.com/XSSheep) (combined)
- [Dyed planks template texture](textures/mcl_decor_dyed_planks.png) uses [oak planks](https://git.minetest.land/MineClone2/MineClone2/src/branch/master/mods/ITEMS/mcl_core/textures/default_wood.png) texture under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) by [XSSheep](https://github.com/XSSheep) (desaturated and lighted up a bit from original)
- [Table Lamp texture](textures/mcl_decor_table_lamp.png) uses [red wool](https://git.minetest.land/MineClone2/MineClone2/src/branch/master/mods/ITEMS/mcl_wool/textures/wool_red.png), [black wool](https://git.minetest.land/MineClone2/MineClone2/src/branch/master/mods/ITEMS/mcl_wool/textures/wool_black.png), [torch](https://git.minetest.land/MineClone2/MineClone2/src/branch/master/mods/ITEMS/mcl_torches/textures/default_torch_on_floor.png), [smooth stone](https://git.minetest.land/MineClone2/MineClone2/src/branch/master/mods/ITEMS/mcl_stairs/textures/mcl_stairs_stone_slab_top.png) and [oak tree](https://git.minetest.land/MineClone2/MineClone2/src/branch/master/mods/ITEMS/mcl_core/textures/default_tree.png) textures under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) by [XSSheep](https://github.com/XSSheep) (combined and resized)
- [Curtains overlay texture](textures/mcl_decor_curtain_overlay.png) uses [top iron pane](https://git.minetest.land/MineClone2/MineClone2/src/branch/master/mods/ITEMS/xpanes/textures/xpanes_top_iron.png) texture under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) by [XSSheep](https://github.com/XSSheep) (resized)

**For code:**

- Sitting function (in api.lua) uses code from [ts_furniture mod](https://content.minetest.net/packages/Thomas-S/ts_furniture/) under [MIT](https://spdx.org/licenses/MIT.html) by [Thomas-S](https://content.minetest.net/users/Thomas-S/)

**Misc:**

- [Homedecor modpack](https://content.minetest.net/packages/VanessaE/homedecor_modpack/) and [xdecor](https://content.minetest.net/packages/jp/xdecor) mods were inspiration for me partly. Thanks to [VanessaE](https://content.minetest.net/users/VanessaE/) and [jp](https://content.minetest.net/users/jp/) for them!
