-- mcl_decor/api.lua

local S = minetest.get_translator(minetest.get_current_modname())

-- originally from the ts_furniture mod (which is from cozy) by Thomas--S // <https://github.com/minetest-mods/ts_furniture/>
mcl_decor.sit = function(pos, _, player)
	local name = player:get_player_name()
	if not player or not name then
		return false
	end
	if not mcl_player.player_attached[name] then
		-- check movement
		if vector.length(player:get_velocity() or player:get_player_velocity()) > 0.125 then
			mcl_title.set(player, "actionbar", {text=S("You have to stop moving before sitting down!"), color="white", stay=60})
			return
		end
		-- TODO: check distance and if the chair is occupied
		-- (maybe needs a function refactor)
		player:move_to(pos)
		player:set_eye_offset({x = 0, y = -7, z = 0}, {x = 0, y = 0, z = 0})
		player:set_physics_override(0, 0, 0)
		mcl_player.player_attached[name] = true
		minetest.after(0.1, function()
			if player then
				mcl_player.player_set_animation(player, "sit" , 30)
			end
		end)
	else
		mcl_decor.stand(player, name)
	end
end
mcl_decor.up = function(_, _, player)
	local name = player:get_player_name()
	if not player or not name then
		return false
	end
	if mcl_player.player_attached[name] then
		mcl_decor.stand(player, name)
	end
end
mcl_decor.stand = function(player, name)
	player:set_eye_offset({x = 0, y = 0, z = 0}, {x = 0, y = 0, z = 0})
	player:set_physics_override(1, 1, 1)
	mcl_player.player_attached[name] = false
	mcl_player.player_set_animation(player, "stand", 30)
end
if not minetest.get_modpath("mcl_cozy") then
	minetest.register_globalstep(function(dtime)
		local players = minetest.get_connected_players()
		for i = 1, #players do
			local player = players[i]
			local name = player:get_player_name()
			local ctrl = player:get_player_control()
			if mcl_player.player_attached[name] and not player:get_attach() and
			(ctrl.up or ctrl.down or ctrl.left or ctrl.right or ctrl.jump or ctrl.sneak) then
				mcl_decor.up(nil, nil, player)
			end
		end
	end)
end
